#include <iostream>
#include <cstdlib>
#include <list>

using namespace std;

struct Process {
	int id;
	int burst_time;
	int waiting_time = 0, response_time = -1, return_time = 0;
	int arrival_time = 0, expulsion_time = 0;

};

void roundRobin(int n_proc, Process *processes, int quantum);
void sjf(int n_proc, Process *processes);
void fcfs(int n_proc, Process *processes);
void printOutput(int n_proc, Process processes[]);
bool sortPredicate(const list<Process>::iterator& first, list<Process>::iterator& second);
list<Process>::iterator map(list<Process>::iterator start, list<Process>::iterator end, int t);


int main()
{

	int n_proc = 0;
	int quantum = 0;

	// Testing values for Round Robin
	// int burst_times[] = {3, 6, 4, 5, 2};
	// int arrival_times[] = {0, 2, 4, 6, 8};

	// Testing values for SJF
	// int burst_times[] = {5, 3, 4, 10, 6};
	// int arrival_times[] = {0, 2, 3, 5, 7};

	cout << "\nCantidad de procesos: ";
	cin >> n_proc;

	Process processes[n_proc];

	for (int i = 0; i < n_proc; i++) {
		processes[i].id = i;

		printf("\nProceso [%d] - Burst time: ", i);
		cin >> processes[i].burst_time;
		
		printf("Proceso [%d] - Tiempo de llegada: ", i);
		cin >> processes[i].arrival_time;
	}

	int option = 0;

	printf("\nEscoge el Algoritmo:\n\n1. Round Robin\n2. SJF\n3. FCFS\n\n~ ");
	cin >> option;

	switch(option) {
		case 1: 
			cout << "\n\nQuantum: ";
			cin >> quantum;
			roundRobin(n_proc, processes, quantum);
			break;
		case 2: sjf(n_proc, processes); break;
		case 3: fcfs(n_proc, processes); break;
		default: break;
	}

	printOutput(n_proc, processes);

	return 0;
}

// Funcion roundRobin: Algoritmo Round Robin
// Parametros:
// int n_proc: numero de procesos
// Process processes[]: arreglo con los procesos
void roundRobin(int n_proc, Process processes[], int quantum)
{
	list<Process> plist(processes, processes + n_proc);
	list<Process>::iterator arrived = plist.begin();

	list<Process> queue;
	queue.push_back(*arrived++);

	Process process;


	int t = 0;

	while (!queue.empty()) {

		printf("\nTIME: %d\n", t);

		process = queue.front();

		if (process.response_time < 0) 
			process.response_time = process.waiting_time = t - process.arrival_time;
		
		else
			process.waiting_time +=  abs(t - process.expulsion_time);
		
		printf("\nProceso [%d] entra al procesador\n", process.id);

		if (process.burst_time > quantum) {
			process.burst_time -= quantum;
			t += quantum;

			process.expulsion_time = t;

			if (arrived -> arrival_time <= t) {
				printf("\nProceso [%d] entra a la cola de procesos listos\n", arrived -> id);

				queue.push_back(*arrived++);
			}

			printf("\nTermina quantum del proceso [%d] entra a la cola de procesos listos", process.id);		
			queue.push_back(process);
		}
		else {
			t += process.burst_time;
			
			processes[process.id].waiting_time = process.waiting_time;
			processes[process.id].response_time = process.response_time;
			processes[process.id].return_time = t - process.arrival_time;
			
			printf("\nTIME: %d\n", t);
			printf("\nProceso [%d] terminado", process.id);
		}

		queue.pop_front();
	
		cout << "\n======================================\n";
	}

	cout << "\n";
}

// Funcion sjf: Algoritmo Short Job First (Sin expulsion)
// Parametros:
// int n_proc: numero de procesos
// Process processes[]: arreglo con los procesos
void sjf(int n_proc, Process processes[]) {

	list<Process> plist(processes, processes + n_proc);

	list<Process>::iterator process;

	int t = 0;

	while(!plist.empty()) {

		printf("\nTIME: %d\n", t);

		process = map(plist.begin(), plist.end(), t);

		printf("\nProceso [%d] entra al procesador\n", process -> id);
		
		if (process -> response_time < 0){
			process -> response_time = process -> waiting_time = t - process -> arrival_time;
		}
			
		else
			process -> waiting_time +=  abs(t - process -> expulsion_time);

		t += process -> burst_time;

		process -> return_time = t - process -> arrival_time;

		printf("\nTIME: %d\n", t);
		printf("\nProceso [%d] terminado", process -> id);
		
		cout << "\n======================================\n";
		processes[process -> id] = *process;

		plist.erase(process);
	}

}

/*
Funcion fcfs: Algoritmo First-Come First-Served
Parametros:
int n_proc: numero de procesos
Process processes[]: arreglo con los procesos
*/
void fcfs(int n_proc, Process processes[]) {
	list<Process> plist(processes, processes + n_proc);
	list<Process>::iterator process = plist.begin();

	int t = 0;

	while(!plist.empty()) {

		printf("\nTIME: %d\n", t);

		printf("\nProceso [%d] entra al procesador\n", process -> id);

		if (process -> response_time < 0)
			process -> response_time = process -> waiting_time = t - process -> arrival_time;
			
		else
			process -> waiting_time +=  abs(t - process -> expulsion_time);

		t += process -> burst_time;

		process -> return_time = t - process -> arrival_time;

		printf("\nTIME: %d\n", t);
		map(++process, plist.end(), t);
		process--;
		printf("\nProceso [%d] terminado", process -> id);

		cout << "\n======================================\n";
		processes[process -> id] = *process;

		plist.erase(process++);
	}

}

// Extra functions

/*
Funcion map: Se usa en SJF. Filtra una lista para encontrar el elemento minimo hasta cierto tiempo
Parametros: 
list<Process>::iterator start: iterador al inicio de la lista
list<Process>::iterator end: iterador al final de la lista
int t: representa el tiempo limite para filtrar
Valor de retorno: Elemento con el menor burst_time en el lapso de tiempo dado
*/
list<Process>::iterator map(list<Process>::iterator start, list<Process>::iterator end, int t) {

	list<list<Process>::iterator> filter;

	while(start != end && start -> arrival_time <= t) {
		
		printf("\nProceso [%d] esta en la cola de procesos listos\n", start -> id);

		filter.push_back(start++);
	}

	filter.sort(sortPredicate);

	return filter.front();
}

/*
Funcion sortPredicate: En realidad es un predicado que sirve para ordenar los elementos de una lista en forma ascendiente
*/
bool sortPredicate(const list<Process>::iterator& first, list<Process>::iterator& second) {

  return (first -> burst_time < second -> burst_time);
}

/*
Funcion printOutput: Imprime los promedios
*/
void printOutput(int n_proc, Process processes[]) {

	float waiting_time = 0, response_time = 0, return_time = 0;

	cout << "\nOutput:\n";

	for (int i = 0; i < n_proc; i++) {
		printf("\nProceso [%d] - Tiempo de espera: %d\tTiempo de respuesta: %d\t\tTiempo de retorno: %d", 
			processes[i].id,
			processes[i].waiting_time,
			processes[i].response_time,
			processes[i].return_time);
	}

	// Average

	for (int i = 0; i < n_proc; i++) {
		waiting_time += processes[i].waiting_time;
		response_time += processes[i].response_time;
		return_time += processes[i].return_time;
	}

	waiting_time /= n_proc;
	response_time /= n_proc;
	return_time /= n_proc;

	cout << "\n\nTiempo promedio:";

	printf("\n\nTiempo de espera: %1f"
		"\nTiempo de respuesta: %f"
		"\nTiempo de retorno: %f\n",
		waiting_time, response_time, return_time);

	std::cout << "Talin es noob\n";
}